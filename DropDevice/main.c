/*
 * DropDevice.c
 *
 * Created: 30.05.2017 11:13:19
 * Author : Alexander Tikunov
 */ 

// define ���
// �������
#define TIMER0_OVF_count 30					// ���������� ���������� TIMER0 �� 1��
#define TIME_READING 2000					// ������ ������ ��������, ��
#define BUTTON_ONTIME 2500					// ����� ����������� ������, ��
#define DRIVERFAULT_ONTIME 200				// ����� ������� ������� fault �������� ��, ��
#define MODBUS_DEADTIME 10					// ����� ����� ��������� ModBus, ��
#define ELEMER_DEADTIME 2000				// �����, ������ ������� �� �����, ��
#define MOVING_TIME 5000					// �����, � ���. �������� �������� ������ ������ ���������, ��
#define MOVING_TIME_FULL 10000				// �����, ������ �������� �� ��������/��������, ��
#define VALVE_OPEN_TIME 10000				// ����� ��������� ��������� �������������, ��
#define CLOSED_DELAY 300					// ����� �������� ����� �������� �������������, ��
#define ENDING_DELAY 10000					// ����� �������� ����� ��������� ����������, ��
#define ENGINE_HEAT_DEADTIME 15000			// �����, � ���. �������� ������ ��������� ���������, ��
enum DRV_MOVING_MODES {DRV_IDLE, DRV_OPEN, DRV_CLOSE};
// ������
#define F_CPU 8000000UL						// ������� ���������������
#define DRIVERFAULT (PIND & (1<<PIND2))	// �������� ������� fault �������� ��
#define BUTTON !(PIND & (1<<PIND3))			// �������� ������� ������
#define CLOSED_PIN !(PIND & (1<<PIND6))		// �������� ��������� ����������� "�������� �������"
#define OPENED_PIN !(PIND & (1<<PIND5))		// �������� ��������� ����������� "�������� �������"
#define MASTERKEY !(PINB & (1<<PINB4))		// �������� ��������� ������-�����
#define VALVE_ON PORTC |= (1<<PINC0)		// ���. ��� VALVE
#define VALVE_OFF PORTC &=~ (1<<PINC0)		// ����. ��� VALVE
#define HEAT_ON PORTC |= (1<<PINC1)			// ���. ��� HEAT
#define HEAT_OFF PORTC &=~ (1<<PINC1)		// ����. ��� HEAT
#define LED_FAULT_ON PORTC |= (1<<PINC3)	// ���. ��� LED_FAULT
#define LED_FAULT_OFF PORTC &=~ (1<<PINC3)	// ����. ��� LED_FAULT
#define DRV_DIR_ON PORTB &=~ (1<<PINB1)		// ���. ��� DRV_DIR
#define DRV_DIR_OFF PORTB |= (1<<PINB1)		// ����. ��� DRV_DIR
#define DRV_ENA_ON PORTB &=~ (1<<PINB0)		// ���. ��� DRV_ENA
#define DRV_ENA_OFF PORTB |= (1<<PINB0)		// ����. ��� DRV_ENA
#define EL4015REQUEST "01030500000c4503"	// �������-������ ������� 4015 �� ������ ���������� �������
#define MODBUS_MAX_LENGTH 40				// ������������ ����� ������
#define JITTER_SENSITIVITY 10				// ���������������� ������������ ����������

//#define DRV_SUPPORT_ENABLE					// �������� �� ��������� ���������� 

// include ���
// ������������ ������������ �����
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <util/delay.h>
// ���������������� ������������ �����
#include "include/ports.h"
#include "include/i2cmaster.h"
#include "include/lcd.h"
#include "include/usart.h"
#include "include/timers.h"
#include "include/1wire.h"

// ���������� ���������� ���
// ModBus
volatile char tempstr_modbus[MODBUS_MAX_LENGTH];	// ��������� ������ ��� ������� ModBus
volatile bool timer_modbus = false;					// ������� �� ������ ModBus
volatile unsigned int ctr_modbus = 0;				// ������� ������� ��� ������� ModBus
volatile unsigned int ctr_msg_modbus = 0;			// ������� ���� ��� ModBus
// ������
//bool elemer_timer = false;							// ������� �� ������ �������� ������ �� �������
// ������� ����������
volatile bool flag_isr_driverfault = false;			// ���� ���������� �� ������ ��������
volatile bool flag_isr_button = false;				// ���� ���������� �� ������
// ������
volatile unsigned int ctr_timer0 = 0;				// ������� ���������� TIMER0 
bool flag_1ms = false;								// ���� 1��
char tempstr[20];									// ��������� ������ ��� �������������� � ������
bool isOpening = false;								// �������� �����������
bool isClosing = false;								// �������� �����������
//unsigned int ctr_elemer_time = 0;					// ������� ������� ��� �������� ������ �� �������
bool drv_fault_monitoring = false;					// ��������� �� ������ ������ fault �������� ��
bool drv_is_heating = false;						// ������� �� �������� ��
volatile char drv_is_moving = DRV_IDLE;				// ����� �������� ��
bool readyGlobal = false;							// ���� "����������"
bool OPENED, CLOSED = false;						// ����� ��������� ����������
char ctr_opened, ctr_closed = 0;					// �������� ������������ ����������
bool skip_reading = false;							// ���� �������� ������ ����� ������
bool drv_allowed = false;							// ��������� �� ���������� ����������

// ��������� ����������
// ���������� �� ����� USART
ISR(USART_RX_vect)
{
	//cli();
	timer_modbus = true;				// ������� ���������� ���� ModBus
	ctr_modbus = 0;						// ����� �������� ������� 
	char tempo = USART_read();
	/*
	if (ctr_msg_modbus == 0)
	{
		if (tempo == 0x01)				// ���� ��������� �� �������, ��������� ������ � �������� �������
		{
			elemer_timer = false;	
			ctr_elemer_time = 0;
		}
	}
	*/
	tempstr_modbus[ctr_msg_modbus] = tempo;		// ���������� ������ � ������ ���������
	ctr_msg_modbus++;
	if(ctr_msg_modbus>=MODBUS_MAX_LENGTH) ctr_msg_modbus = MODBUS_MAX_LENGTH-1;
	//sei();
}

// ���������� �������
ISR(TIMER0_OVF_vect)
{
	//cli();
	ctr_timer0++;
	//sei();
}

// ������� ���������� - ������ ��������
ISR(INT0_vect)
{
	//cli();
	if (drv_fault_monitoring) flag_isr_driverfault = !DRIVERFAULT;	// ���� ������� ����������, ��������� ��������� ����
	else flag_isr_driverfault = false;								// ���� �������� - ������ ���
	//sei();
}

// ������� ���������� - ������ ������
ISR(INT1_vect)
{
	//cli();
	flag_isr_button = BUTTON;
	//sei();	
}

void DRV_SET_ENA(bool active) {
	if (active)	{
		DRV_ENA_ON;
		//flag_isr_driverfault = DRIVERFAULT;		// ��� ��������� ���������, ���� �� ������
	} else DRV_ENA_OFF;
}

void DRV_SET_MODE(int mode) {
	if (mode != DRV_IDLE) 
	{
		drv_fault_monitoring = true;		
	} else
	{
		drv_fault_monitoring = false;
	}
	drv_is_moving = mode;
}

// ������� ������� �������
void SendEl4015Request()
{
	USART_set_direction(USART_WRITE);
	USART_write_hex(EL4015REQUEST);
	USART_set_direction(USART_READ);
	//elemer_timer = true;
}

// TODO �������������� ����������� ����������� ������ ��������, ��������� <avr/pgmspace.h>
// ������� ������ ��������� �� �������
void PrintOneWireError()
{		
	LCD_print_line("������ �����������", 1);
	LCD_print_line("OneWire", 2);
	LCD_print_line("�� ���������!", 3);
	LCD_print_line("��������� ����������", 4);	
}

void PrintElemerError()
{
	LCD_clear_screen();
	LCD_print_line("������ ������-4015", 1);
	LCD_print_line("�� ���������!", 2);
	LCD_print_line("��������� ����������", 4);
}

void PrintUnknownPositionError()
{
	LCD_clear_screen();
	LCD_print_line("��������� ��������", 1);
	LCD_print_line("�� ����������!", 2);
	LCD_print_line("��������� ���. ����!", 3);
}

void PrintShredingerPositionError()
{
	LCD_print_line("���������� ��������", 1);
	LCD_print_line("���������!", 2);
	LCD_print_line("��������� ��������", 3);
	LCD_print_line("����������� ��������", 4);
}

void PrintInitClosingMessage()
{
	LCD_clear_screen();
	LCD_print_line("�������� �����������!", 2);
}

void PrintEndingErrorTemplate()
{	
	LCD_print_line("�2=       �5=       ", 3);
	LCD_print_line("�3=       �6=       ", 4);
}

void PrintDriverFaultError()
{	
	LCD_print_line("����� �������� ��!", 1);
	if (drv_is_moving == DRV_OPEN)
	{
		LCD_print_line("��������� ���������!", 2);
		LCD_print_line("��������� ��������", 3);
		LCD_print_line("������� ��������.", 4);		
	}
	else if (drv_is_moving == DRV_CLOSE)
	{
		LCD_print_line("��������� ���������!", 2);
		PrintEndingErrorTemplate();
	}
	else
	{
		LCD_print_line("��������� � �����", 2);
		LCD_print_line("�������� �������,", 3);
		LCD_print_line("��������� ���������.", 4);
	}
}

void PrintHeatTemplate()
{
	LCD_print_line("�������� �������", 1);
	LCD_print_line("�1=       �4=       ", 2);
	LCD_print_line("�2=       �5=       ", 3);
	LCD_print_line("�3=       �6=       ", 4);
}

void PrintJamError()
{
	LCD_clear_screen();
	LCD_print_line("�������� ��������", 1);
	LCD_print_line("������������", 2);
	LCD_print_line("��������", 3);
}

void PrintHeatingError()
{	
	LCD_clear_screen();
	LCD_print_line("�������� �� ��������", 1);
	LCD_print_line("��������� �������-", 3);
	LCD_print_line("������� �������!", 4);
}

// ����� ���� ��������, ���������, ������� � �����
void ResetAll()
{
	timer_modbus = false;
	ctr_modbus = 0;
	ctr_msg_modbus=0;
//	elemer_timer = false;
//	ctr_elemer_time = 0;
	isOpening = false;
	isClosing = false;
	readyGlobal = false;
	drv_is_heating = false;
// 	DRV_ENA_OFF;
// 	DRV_PUL_SetMode(DRV_PUL_0Gz);
	DRV_SET_MODE(DRV_IDLE);
	drv_allowed = false;
	skip_reading = false;
	VALVE_OFF;
}

// main
int main(void)
{	
	// enum'� ���
	// ������ �������� �������� �����
	// TODO �������������� ��������� ������� ������
	enum WorkStates {WORK_IDLE, WORK_RESET, WORK_SENSORS_INIT, WORK_INIT, WORK_READY, WORK_READY_CYCLE, WORK_521, WORK_522,
		WORK_522_CYCLE, WORK_523, WORK_524, WORK_528, WORK_528W, WORK_529, WORK_5210, WORK_5211};
	// ������ ���� LED_FAULT: ����, ������, ��������
	enum LED_FAULT_States {LED_FAULT_0Gz, LED_FAULT_05Gz, LED_FAULT_1Gz, LED_FAULT_5Gz, LED_FAULT_FULL};
	// ��������� ����������
	// ������ ������
	int currentWorkState = 0;						// ������� ���������
	int LED_FAULT_Mode = 0;							// ����� ������ ���� LED_FAULT
	// ������
	union {char c[4]; float f;}
		IEEE754_Converter;							// union ��� �������������� IEEE754 �� float
	float elemerdata[6];							// ������ �� ���������� �� ������ ������ 4015
	bool elemernewdata = false;						// ����, ���� �� ����� ������ �� �������
	//bool elemer_connection_error = false;			// ���� ������ ����� � ��������
	// ������� ����������	
	unsigned int ctr_isr_driverfault = 0;			// ������� ���������� �� ������ ��������
	unsigned int ctr_isr_button = 0;				// ������� ���������� �� ������
	// OneWire
	//bool onewire_error = false;						// ���� ������ ����� � OneWire
	//char onewire_error_counter = 0;					// ������� ������ ����� � OneWire
	bool onewirenewdata = false;					// ����, ���� �� ����� ������ �� OneWire
	char onewirebuffer[10];							// ������ ������ � ������� �7 � ���� ������
	float onewiretemperature = 0;					// ����������� �7
	// ������
	bool sensors_reading = false;					// ���������� �� �������
	unsigned int ctr_reading = 0;					// ������� ��� ������� ������ ��������
	unsigned int ctr_LED = 0;						// ������� ��� ������� LED ���������� "������"
	unsigned int ctr_reset_time = 0;				// ������� ������
	unsigned int ctr_heat_wait = 0;					// ������� �������� �������
	unsigned int ctr_open_wait = 0;					// ������� �������� �������� ��������
	// ������
	bool engineHeating = false;						// ������� �� �������� ��
	
	// �������������
	PORTS_init();
	USART_init();
	TIMER0_init();
	TIMER1_init();
	i2c_init();
	LCD_init();
	LCD_clear_screen();
	EXTERNAL_INTERRUPTS_init();	
	sei();		// ���������� ���������� ����������
			
	for (int i = 0; i<6; i++) elemerdata[i] = 0.0;
	LED_FAULT_Mode = LED_FAULT_0Gz;
	currentWorkState = WORK_SENSORS_INIT;
	
	// ����������� ����
    while (1) 
    {		
		if (ctr_timer0 > TIMER0_OVF_count) // ������� ���������� ���������� TIMER0
		{				
			flag_1ms = true;
			ctr_timer0 -= TIMER0_OVF_count;
		}		
		
		
		
		// ������ ��
		if (drv_allowed)
		{
			switch (drv_is_moving)
			{
				case DRV_OPEN:
				DRV_PUL_SetMode(DRV_PUL_100Gz);
				DRV_DIR_OFF;
				break;
			
				#ifdef DRV_SUPPORT_ENABLE					
					case DRV_CLOSE:
					if (!CLOSED)
					{
						DRV_PUL_SetMode(DRV_PUL_100Gz);
						DRV_DIR_ON;
					}
					else
					{
						DRV_PUL_SetMode(DRV_PUL_0Gz);
						DRV_DIR_ON;
					}
					break;
				#else
					case DRV_CLOSE:
					DRV_PUL_SetMode(DRV_PUL_100Gz);
					DRV_DIR_ON;
					break;						
				#endif
						
				default:
				DRV_PUL_SetMode(DRV_PUL_0Gz);
				DRV_DIR_OFF;
				break;
			}
		
			if (drv_is_heating || drv_is_moving != DRV_IDLE)
			{
				DRV_SET_ENA(true);
			}
			else
			{
				DRV_SET_ENA(false);
			}
		
		}
		// END ������ ��
		
		// ���������� ��������
		if (OPENED_PIN) 
		{
			if (ctr_opened < JITTER_SENSITIVITY)
			{
				ctr_opened++;
			}
			else
			{
				OPENED = true;
			}
		}
		else
		{
			if (ctr_opened > 0)
			{
				ctr_opened--;
			}
			else
			{
				OPENED = false;
			}
		}
		
				
		
		if (CLOSED_PIN) 
		{
			if (ctr_closed < JITTER_SENSITIVITY)
			{
				ctr_closed++;
			}
			else
			{
				CLOSED = true;
			}
		}
		else
		{
			if (ctr_closed > 0)
			{
				ctr_closed--;
			}
			else
			{
				CLOSED = false;
			}
		}
		// END ���������� ��������
		
		if (flag_1ms)
		{
			// ������ 1��
			flag_1ms = false;			
			ctr_reading++;
			
			// ��������� ��������� ModBus
			if(timer_modbus) 
			{				
				ctr_modbus++; // ������� ���-�� �� ����� ���������� �����
				if(ctr_modbus >= MODBUS_DEADTIME)
				{
					timer_modbus = false;	// ��������� ������ ModBus
					ctr_modbus = 0;			// ����� �������� �������
					if(tempstr_modbus[0] == 0x01 && tempstr_modbus[1] == 0x03) // ��������� �� ������� 4015
					{						
						if (ctr_msg_modbus == 29) // ������ ���� ������ 29 ����
						{
							// ��������� ������� � ������� �� ��� ������
							int index = 3; // ������ ���������� � 3 �����, �� 6 ���� �� 4 �����
							for (int i=0; i<6; i++)
							{								
								IEEE754_Converter.c[3] = tempstr_modbus[index];
								IEEE754_Converter.c[2] = tempstr_modbus[index+1];
								IEEE754_Converter.c[1] = tempstr_modbus[index+2];
								IEEE754_Converter.c[0] = tempstr_modbus[index+3];
								elemerdata[i] = IEEE754_Converter.f; // �������� ������ � ������
								index += 4;
							}
							elemernewdata = true;
							drv_allowed = true;
						}
					}
					ctr_msg_modbus=0;
				}
			} // END ��������� ��������� ModBus	
										
			// ������� LED ���������� "������"
			switch (LED_FAULT_Mode)
			{
				case LED_FAULT_05Gz:
				ctr_LED++;
					if (ctr_LED<=2000) LED_FAULT_ON;
					else if (ctr_LED<=4000) LED_FAULT_OFF;
					else ctr_LED=0;
				break;
				case LED_FAULT_1Gz:
					ctr_LED++;
					if (ctr_LED<=1000) LED_FAULT_ON;
					else if (ctr_LED<=2000) LED_FAULT_OFF;
					else ctr_LED=0;
				break;
				case LED_FAULT_5Gz:
					ctr_LED++;
					if (ctr_LED<=400) LED_FAULT_ON;
					else if (ctr_LED<=800) LED_FAULT_OFF;
					else ctr_LED=0;
				break;				
				case LED_FAULT_FULL:
					LED_FAULT_ON;					
				break;
				default:
					ctr_LED=0;
					LED_FAULT_OFF;
				break;
			}
			// END ������� LED ���������� "������"
						
			// ��������� ���������� �� ������� ������
			if(flag_isr_button) 
			{
				if(ctr_isr_button < BUTTON_ONTIME)
				{
					ctr_isr_button++;
				} else
				{
					flag_isr_button = false;
					
					// ���� ������ ����� � ���. BUTTON_ONTIME
					if (currentWorkState == WORK_READY_CYCLE && readyGlobal) 
					{
						currentWorkState = WORK_523;						
					} 
					else if (currentWorkState == WORK_IDLE ||
					currentWorkState == WORK_5210 || currentWorkState == WORK_5211) 
					{
						currentWorkState = WORK_RESET;
					}
				}
			} else ctr_isr_button = 0;
			// END ��������� ���������� �� ������� ������
						
			// ��������� �������� �������� �����
			if (currentWorkState == WORK_IDLE) // ����� ��������. ��� ����������, ����� �� �����������
			{
				if (LED_FAULT_Mode == LED_FAULT_0Gz) LED_FAULT_Mode = LED_FAULT_FULL;
				ResetAll();
				sensors_reading = false;	
				ctr_open_wait = 0;
				ctr_heat_wait = 0;
			}
			else if (currentWorkState == WORK_RESET) // ����� ������. ��������� ��� ��������� ������ ����� ������
			{										 // ��� ����� ��������� ���������� ������
				if (ctr_reset_time == 0)
				{
					LED_FAULT_Mode= LED_FAULT_0Gz;
					LED_FAULT_OFF;
					ResetAll();
					sensors_reading = false;
					ctr_open_wait = 0;
					ctr_heat_wait = 0;
					LCD_clear_screen();
					LCD_print_line("����� ���������", 2);
				}
				ctr_reset_time++;
				if (ctr_reset_time > 2000)
				{
					LCD_clear_screen();
					ctr_reset_time = 0;
					currentWorkState = WORK_SENSORS_INIT;
				}
			}
			else if (currentWorkState == WORK_SENSORS_INIT) // ����� ������������� ��������. ����� ��� ������������ ������ ��� ����� �������������
			{
				LCD_print_line("�������������", 1);
				
				cli();
				float newonewiretemperature = therm_read_temperature(onewirebuffer);
				/*if (newonewiretemperature == 1000) onewire_error = true;
				else */if (newonewiretemperature < 100) // ������ ������ ���� �� +100 ��������
				{
					onewiretemperature = newonewiretemperature;
					onewirenewdata = true;
				}
				sei();
				SendEl4015Request();
				
				currentWorkState = WORK_INIT;
			}
			else if (currentWorkState == WORK_INIT) // ����� �������������
			{
				//if (!elemer_timer) { // ���� �� ���� ������ �� �������, ������ �� ������.
									 // ������ ������� ��� �� ��������������, �.�. ��� ��� ���� ��������� ��������� ����
					if (OPENED && CLOSED) // ���� �������� ���������
					{						
						PrintShredingerPositionError();
						LED_FAULT_Mode = LED_FAULT_05Gz;
						currentWorkState = WORK_IDLE;
					} else
					{
						sensors_reading = true; // �������� �����
						currentWorkState = WORK_READY;
					}
					
					if (DRIVERFAULT) // ��������� ������� ������ ��������. ��� ����� �������, �.�. ���� ���������� �� ������
					// �������� ��� ���� ���������� �� ����������� ���������, ��� �� ������������ ��������
					{
						PrintDriverFaultError();
						currentWorkState = WORK_IDLE;
					}
				//}
			}
			else if (currentWorkState == WORK_READY) // ����������� ����� �������� ����� ��� ������ � ��������
			{
				LCD_print_line(" �����!   �4=       ", 1);
				LCD_print_line("�1=       �5=       ", 2);
				LCD_print_line("�2=       �6=       ", 3);
				LCD_print_line("�3=       �7=       ", 4);
				
				ctr_open_wait = 0;
				ctr_heat_wait = 0;
				
				if (MASTERKEY)
				{
					if (!OPENED) LCD_print("��������  ");
					else LCD_print("�������   ");
				}/*
				else
				{
					if (!CLOSED) LCD_print("��������  ");
					else readyGlobal = true;
					
				}*/
				
				HEAT_ON;
				currentWorkState = WORK_READY_CYCLE;
			}
			else if (currentWorkState == WORK_READY_CYCLE) // ����������� ����� ����� ������ �� �����
			{
				if (elemernewdata)
				{
					int x,y = 0;
					for (int i=0; i<6; i++)
					{
						if (i<3) x=4; else x=14;
						if (i<3) y=i+2; else y=i-2;
						LCD_move_cursor(x, y);
						sprintf(tempstr, "%+5.1f�", elemerdata[i]);
						LCD_print(tempstr);
					}
					elemernewdata = false;
				}
				if (onewirenewdata)
				{
					LCD_move_cursor(14, 4);
					LCD_print(onewirebuffer);
					onewirenewdata = false;
				}
				/*
				if (MASTERKEY)	
				{
					DRV_SET_MODE(DRV_CLOSE_SUPPORT);
				}
				else
				{
					DRV_SET_MODE(DRV_IDLE);
				}
				*/
				
				/*if (!drv_is_heating) 
				{
					ctr_heat_wait = 0;*/
					if (MASTERKEY) 
					{
						readyGlobal = false;
						isClosing = false;
						if (OPENED && isOpening)
						{
							DRV_SET_MODE(DRV_IDLE);
							ctr_open_wait = 0;
							isOpening = false;
							engineHeating = false;
							LCD_cursor_return();
							LCD_print("�������   ");
						}
						else if (!OPENED && !isOpening)
						{
							DRV_SET_MODE(DRV_OPEN);
							isOpening = true;
							LCD_cursor_return();
							LCD_print("��������  ");				
						}
					}
					else
					{
						#ifdef DRV_SUPPORT_ENABLE
						DRV_SET_MODE(DRV_CLOSE);
						#endif
						isOpening = false;						
						if (DRIVERFAULT && isClosing)
						{
							#ifndef DRV_SUPPORT_ENABLE
							DRV_SET_MODE(DRV_IDLE);
							#endif
							isClosing = false;
							ctr_open_wait = 0;
							engineHeating = false;	
							readyGlobal = true;
							LCD_cursor_return();
							LCD_print(" �����!   ");
						}
						else if (!isClosing && !readyGlobal)
						{
							#ifndef DRV_SUPPORT_ENABLE
							DRV_SET_MODE(DRV_CLOSE);
							#endif
							isClosing = true;
							LCD_cursor_return();
							LCD_print("��������  ");
						}
					}						
				/*}*/
				/*
				if (isOpening || isClosing)
				{
					if (ctr_open_wait < MOVING_TIME_FULL)
					{
						ctr_open_wait++;
					}
					else
						{
							if (!engineHeating && onewiretemperature < 3.0)
							{
								engineHeating = true;
								drv_is_heating = true;
								ctr_open_wait = 0;
								LCD_cursor_return();
								LCD_print("��������  ");
							}
							else 
							{
								engineHeating = false;
								drv_is_heating = false;
								ctr_open_wait = 0;
								ctr_heat_wait = 0;
								PrintJamError();
								currentWorkState = WORK_IDLE;
							}
						}
				}
				
				if (drv_is_heating)
				{					
					if (ctr_heat_wait < ENGINE_HEAT_DEADTIME)
					{
						ctr_heat_wait++;
					}
					else
					{
						ctr_open_wait = 0;
						ctr_heat_wait = 0;
						drv_is_heating = false;
						isOpening = false;
						isClosing = false;
					}
				}
						*/		
			}
			else if (currentWorkState == WORK_521)		// ������ ��� �������� �����, �������� ��
			{
				if (onewiretemperature < 3 && !drv_is_heating)	// ���� �������� ��������, � ����������� ���� ����,
				{												// �������� �������� � ������� ������ ��� ���������
					drv_is_heating = true;
					LCD_print_line("�������� �� �������", 1);
					LCD_print_line("�1=       �4=       ", 2);
					LCD_print_line("�2=       �6=       ", 3);
					LCD_print_line("�3=       �7=       ", 4);					
				}
				if (drv_is_heating) // ���� ������� ��������, ��������� ������ �� ������
				{
					ctr_heat_wait++;
					if (elemernewdata)
					{						
						if (!drv_fault_monitoring)
							{
							for (int i=0; i<3; i++)
							{
								int y = 0;
								y=i+2;
								LCD_move_cursor(4, y);
								sprintf(tempstr, "%+5.1f�", elemerdata[i]);
								LCD_print(tempstr);
							}
							LCD_move_cursor(14, 2);
							sprintf(tempstr, "%+5.1f�", elemerdata[3]);
							LCD_print(tempstr);						
							LCD_move_cursor(14, 3);
							sprintf(tempstr, "%+5.1f�", elemerdata[5]);
							LCD_print(tempstr);
						}
						elemernewdata = false;
					}
					if (onewirenewdata)
					{
						LCD_move_cursor(14, 4);
						LCD_print(onewirebuffer);
						if (onewiretemperature > 7) // ���� ��������� ���� ����, ��������� ��������
						{							// � ��������� �� ��������� ���
							drv_is_heating = false;
							currentWorkState = WORK_522;
						}
						onewirenewdata = false;
					}
					if (ctr_heat_wait > ENGINE_HEAT_DEADTIME) // ���� �������� ��� ������� �����-�� �����, �� �� �� �� ��������
					{
						LCD_clear_screen();
						LCD_print_line("�������� �� ��������", 1);
						LCD_print_line("������ �����������", 2);
						LCD_print_line("���������", 3);						
						currentWorkState = WORK_IDLE;
					}					
				}
				if (onewiretemperature >= 3 && !drv_is_heating) // ���� �������� ��������, � ����������� ���� ����, ��������� �� ��������� ���
				{
					currentWorkState = WORK_523;
				}
			}			
			else if (currentWorkState == WORK_523) // �������� ��������, ����� �������
			{
				ctr_heat_wait = 0;
				ctr_open_wait = 0;
				isOpening = true;
				DRV_SET_MODE(DRV_OPEN);
				LCD_clear_screen();
				LCD_print_line("�������� ��������", 1);
				LCD_print_line("�������� �����������", 2);
				LCD_print_line("������������ ������", 3);
				LCD_print_line("�1=       �6=       ", 4);
				LCD_move_cursor(4, 4);
				sprintf(tempstr, "%+5.1f�", elemerdata[0]);
				LCD_print(tempstr);
				LCD_move_cursor(14, 4);
				sprintf(tempstr, "%+5.1f�", elemerdata[5]);
				LCD_print(tempstr);
				currentWorkState = WORK_528;				
			}
			else if (currentWorkState == WORK_524)  // ���������� ������ ���� �������� �����������
			{// ���� �� ��������
				if (ctr_open_wait < MOVING_TIME) 
				{
					ctr_open_wait++;
					if (!OPENED && !CLOSED) // �������� �������
					{
						currentWorkState = WORK_528;
						ctr_heat_wait = 0;
						ctr_open_wait = 0;
					}
				}
				else
				{
					PrintJamError();	
					currentWorkState = WORK_IDLE;			
				}
			}
			else if (currentWorkState == WORK_528) // ���� ���� �������� "������ �� ��������" � ��������� ������������ 
			{
				ctr_open_wait++;
				if (OPENED)
				{
					ctr_open_wait = 0;
					DRV_SET_MODE(DRV_IDLE);
					if (ctr_heat_wait < 150)
					{
						ctr_heat_wait++;						
					}
					else
					{
						VALVE_ON;
						ctr_heat_wait = 0;
						LCD_print_line("�������� �������", 1);
						LCD_print_line("������������ ������", 2);
						LCD_print_line("�1=       �4=       ", 3);
						LCD_print_line("�2=       �6=       ", 4);
						currentWorkState = WORK_528W;						
					}
				}
				if (ctr_open_wait > MOVING_TIME_FULL)
				{
					PrintJamError();
					currentWorkState = WORK_IDLE;
				}
			}
			else if (currentWorkState == WORK_528W) // ������������ ������. ���� ������� ������ � ��������� ������ �� ������
			{
				//ctr_open_wait++;
				if (elemernewdata)
				{
					if (!drv_fault_monitoring)
					{
						LCD_move_cursor(4, 3);
						sprintf(tempstr, "%+5.1f�", elemerdata[0]);
						LCD_print(tempstr);
						LCD_move_cursor(14, 3);
						sprintf(tempstr, "%+5.1f�", elemerdata[1]);
						LCD_print(tempstr);
						LCD_move_cursor(4, 4);
						sprintf(tempstr, "%+5.1f�", elemerdata[3]);
						LCD_print(tempstr);
						LCD_move_cursor(14, 4);
						sprintf(tempstr, "%+5.1f�", elemerdata[5]);
						LCD_print(tempstr);					
					}
					elemernewdata = false;
				}
				if (flag_isr_button) // ��������� ������
				{
					flag_isr_button = false;
					VALVE_OFF;
					LCD_print_line("������������ ������", 2);
					ctr_open_wait = 0;
					currentWorkState = WORK_529;
				}
			}
			else if (currentWorkState == WORK_529) // ��������� ��������
			{
				ctr_open_wait++;				
				if (ctr_open_wait == CLOSED_DELAY)
				{
					DRV_SET_MODE(DRV_CLOSE);
					isClosing = true;
					LCD_print_line("�������� �����������", 1);		
				}
				if (DRIVERFAULT && !OPENED)
				{
					DRV_SET_MODE(DRV_IDLE);
					isClosing = false;
					ctr_open_wait = 0;
					LCD_print_line("����� ��������", 1);
					currentWorkState = WORK_5210;
				}
				if (ctr_open_wait > CLOSED_DELAY + MOVING_TIME_FULL)
				{
					LCD_print_line("����� ��������", 1);
					LCD_print_line("��������� ��������!", 2);
					PrintEndingErrorTemplate();
					LED_FAULT_Mode=LED_FAULT_FULL;
					currentWorkState = WORK_5211;					
				}
			}
			else if (currentWorkState == WORK_5210) // ����� ��������. ���� �������, ������� ������� ��� ������ � ��������� ������ �� ������
			{
				if (ctr_open_wait == 0)
				{
					LCD_print_line("�������� �� ��������", 2);
					LCD_print_line("�������� �������", 3);
					LCD_print_line("������������ ������", 4);				
				}				
				if (ctr_open_wait == ENDING_DELAY)
				{				
					LCD_print_line("�1=       �4=       ", 2);
					LCD_print_line("�2=       �5=       ", 3);
					LCD_print_line("�3=       �6=       ", 4);	
					ctr_open_wait = ENDING_DELAY + 1;	
				}
				if (ctr_open_wait <= ENDING_DELAY) ctr_open_wait++;
				else
				{
					if (elemernewdata)
					{						
						if (!drv_fault_monitoring)
						{
							int x,y = 0;
							for (int i=0; i<6; i++)
							{
								if (i<3) x=4; else x=14;
								if (i<3) y=i+2; else y=i-1;
								LCD_move_cursor(x, y);
								sprintf(tempstr, "%+5.1f�", elemerdata[i]);
								LCD_print(tempstr);
							}
						}
						elemernewdata = false;
					}
				}
				
			}
			else if (currentWorkState == WORK_5211) // ���������� ������ ���� ��������� �������� ����� ������
			{
				if (elemernewdata)
				{					
					if (!drv_fault_monitoring)
					{
						LCD_move_cursor(4, 3);
						sprintf(tempstr, "%+5.1f�", elemerdata[0]);
						LCD_print(tempstr);
						LCD_move_cursor(14, 3);
						sprintf(tempstr, "%+5.1f�", elemerdata[3]);
						LCD_print(tempstr);
						LCD_move_cursor(4, 4);
						sprintf(tempstr, "%+5.1f�", elemerdata[4]);
						LCD_print(tempstr);
						LCD_move_cursor(14, 4);
						sprintf(tempstr, "%+5.1f�", elemerdata[5]);
						LCD_print(tempstr);					
					}
					elemernewdata = false;
				}
			}
			// END ��������� �������� �������� �����
			// ��������� ������	
			// ��������� ���������� �� ������ ��������
			/*if(flag_isr_driverfault)
			{
				if(ctr_isr_driverfault < DRIVERFAULT_ONTIME)
				{
					ctr_isr_driverfault++;
				} else
				{
					flag_isr_driverfault = false;
					
					// ��� ������ ������ �������� � ���. 200��
					if (currentWorkState != WORK_IDLE || currentWorkState != WORK_5210 || currentWorkState != WORK_5211)
					{
						PrintDriverFaultError();
						LED_FAULT_Mode = LED_FAULT_1Gz;
						currentWorkState = WORK_IDLE;
					}
					
				}
			} else ctr_isr_driverfault = 0;*/
			// END ��������� ���������� �� ������ ��������
			/*
			// ��������� ���������� ����� � ��������
			if (elemer_timer) // ���� ������ �������
			{
				ctr_elemer_time++; //������� ��������� �����
				if (ctr_elemer_time > ELEMER_DEADTIME) // ���� ������ ������ ��� ELEMER_DEADTIME
				{
					// ������������� ������ � ���������� ���� ������
					elemer_timer = false;
					ctr_elemer_time = 0;
					elemer_connection_error = true;
				}
			}
			if (elemer_connection_error)
			{
				PrintElemerError();
				elemer_connection_error = false;
				currentWorkState = WORK_IDLE;
			}
			// END ��������� ���������� ����� � ��������
			*/
			/*
			// ��������� ���������� ����� � OneWire
			if (onewire_error)
			{
				PrintOneWireError();
				onewire_error = false;
				currentWorkState = WORK_IDLE;				
			}
			// END ��������� ���������� ����� � OneWire
			// END ��������� ������
			*/
		} // END ������ 1��
		
		if (ctr_reading >= TIME_READING)
		{
			// ����� ��������
			ctr_reading -= TIME_READING;
							
			skip_reading = false;
			
			#ifdef DRV_SUPPORT_ENABLE
				if (((drv_is_moving == DRV_CLOSE && CLOSED) || drv_is_heating) && drv_allowed)
				{
					skip_reading = true;
					DRV_ENA_OFF;
					DRV_DIR_OFF;
					DRV_PUL_SetMode(DRV_PUL_0Gz);
					drv_allowed = false;
					
				}
				if ((drv_is_moving == DRV_CLOSE && !CLOSED) || drv_is_moving == DRV_OPEN)
				{
					skip_reading = true;
				}
			#else
				if (drv_is_moving != DRV_IDLE || drv_is_moving == DRV_OPEN)
				{
					skip_reading = true;
				}			
			#endif			
			
			if (sensors_reading && !skip_reading)
			{
				cli();
				float newonewiretemperature = therm_read_temperature(onewirebuffer);
				/*
				if (newonewiretemperature == 1000 && !drv_fault_monitoring) 
				{
					if (onewire_error_counter < 3) onewire_error_counter++;
				}
				else
				{
					if (onewire_error_counter > 1) onewire_error_counter--;
				}
				if (onewire_error_counter >= 3)
				{
					onewire_error = true;
					onewire_error_counter = 0;
				}
				
				else */if (newonewiretemperature < 100) // ������ ������ ���� �� +100 ��������
				{
					onewiretemperature = newonewiretemperature;
					onewirenewdata = true;		
				}
				sei();				
				SendEl4015Request();
			}
					
		} // END ����� ��������					
		
    }
	
}

