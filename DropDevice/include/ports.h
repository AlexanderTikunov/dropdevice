/*
 * ports.h
 *
 * Created: 30.05.2017 15:13:52
 *  Author: Alexander Tikuov
 */ 

#ifndef PORTS_H_
#define PORTS_H_

#include <stdbool.h>

// ������������� ������ �����/������
void PORTS_init()
{
	// ���� B
	DDRB=(1<<DDB7) | (1<<DDB6) | (1<<DDB5) | (0<<DDB4) | (0<<DDB3) | (1<<DDB2) | (1<<DDB1) | (1<<DDB0);	
	PORTB = (1<<PINB4);	// �������� ������-�����
	// ���� C
	DDRC=(0<<DDC6) | (1<<DDC5) | (1<<DDC4) | (1<<DDC3) | (0<<DDC2) | (1<<DDC1) | (1<<DDC0);

	// ���� D
	DDRD=(0<<DDD7) | (0<<DDD6) | (0<<DDD5) | (1<<DDD4) | (0<<DDD3) | (0<<DDD2) | (1<<DDD1) | (0<<DDD0);
}

// ������������� ������� ����������
void EXTERNAL_INTERRUPTS_init() 
{
	// �� ��������� ���������
	EICRA=(0<<ISC11) | (1<<ISC10) |(0<<ISC01) |(1<<ISC00);
	// ��� ���������� ���������� ��������
	EIMSK=(1<<INT1) | (1<<INT0);
}
/*
// �������� ��� ��������� ��� ����� B
void PORTB_switch_pin(bool on, int pin)
{
	if (on) PORTB |= (1<<pin); else PORTB &=~ (1<<pin);
}

// ����������� ��� ����� B
void PORTB_toggle_pin(int pin)
{
	PORTB =~ PORTB & (1<<pin);
}

// �������� ��� ��������� ��� ����� C
void PORTC_switch_pin(bool on, int pin)
{
	if (on) PORTC |= (1<<pin); else PORTC &=~ (1<<pin);
}

// ����������� ��� ����� C
void PORTC_toggle_pin(int pin)
{
	PORTC =~ PORTC & (1<<pin);
}
*/
// �������� ��� ��������� ��� ����� D
void PORTD_switch_pin(bool on, int pin)
{
	if (on) PORTD |= (1<<pin); else PORTD &=~ (1<<pin);
}
/*
// ����������� ��� ����� D
void PORTD_toggle_pin(int pin)
{
	PORTD =~ PORTD & (1<<pin);
}
*/
#endif /* PORTS_H_ */