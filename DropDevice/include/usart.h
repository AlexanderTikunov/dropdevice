/*
 * usart.h
 *
 * Created: 31.05.2017 15:48:49
 *  Author: Alexander Tikunov
 */ 

#ifndef USART_H_
#define USART_H_

#include <string.h>
#include "ports.h"

enum USART_dir {USART_READ, USART_WRITE};

// ����������� ���, ���������� �� ����������� ������ (��� �������� ������������ enum USART_dir)
void USART_set_direction(bool dir) 
{
	_delay_ms(1);
	PORTD_switch_pin(dir, PIND4);
}

// ������������� USART
void USART_init()
{
	// ���������: 8 Data, 1 Stop, No Parity
	// ��������: ���
	// ����������: ���
	// �����: �����������
	// ��������: 9600
	UBRR0H=0x00;
	UBRR0L=0x33;
	UCSR0A=(0<<RXC0) | (0<<TXC0) | (0<<UDRE0) | (0<<FE0) | (0<<DOR0) | (0<<UPE0) | (0<<U2X0) | (0<<MPCM0);
	UCSR0B=(1<<RXCIE0) | (0<<TXCIE0) | (0<<UDRIE0) | (1<<RXEN0) | (1<<TXEN0) | (0<<UCSZ02) | (0<<RXB80) | (0<<TXB80);
	UCSR0C=(0<<UMSEL01) | (0<<UMSEL00) | (0<<UPM01) | (0<<UPM00) | (0<<USBS0) | (1<<UCSZ01) | (1<<UCSZ00) | (0<<UCPOL0);
}

// �������� USART
void USART_write_ascii(char *data)
{
	unsigned int len = strlen(data);
	for (int i = 0; i<len; i++)
	{
		while ( !( UCSR0A & (1<<UDRE0)) );
		UDR0 = data[i];
	}
	while ( !( UCSR0A & (1<<UDRE0)) );
}

// �������� ������ � USART � HEX ����
void USART_write_hex(char *data)
{
	unsigned int len = strlen(data);
	for (int i=0; i<len/2; i++)
	{
		char tempstr[2];
		tempstr[0] = data[2*i];
		tempstr[1] = data[2*i+1];
		unsigned int value = (unsigned int) strtol(tempstr, NULL, 16);
		while ( !( UCSR0A & (1<<UDRE0)) );
		UDR0 = value;
	}
	while ( !( UCSR0A & (1<<UDRE0)) );
}

// ���� USART
char USART_read()
{
	while(!(UCSR0A & (1<<RXC0)));
	return UDR0;
}

#endif /* USART_H_ */