/*
 * lcd.h
 *
 * Created: 31.05.2017 17:59:34
 *  Author: Alexander Tikunov
 */ 


#ifndef LCD_H_
#define LCD_H_

#include <string.h>

#define PCF8574t 0x4e				// ����� ����������� ��� �� ���� i2c
enum LCDCD {LCDCOMMAND, LCDDATA};	// ��� ����������� ��� ����������� �������� LCD_write

#include "i2cmaster.h"

bool lcd_isrwon = false;		// ���� �� ��������� ��������
bool lcd_isledon = true;		// ��������� �� ��������� ��������

// ������ ������� ��� ������ � ���
// ������� ��� ������ ������������ ������ isdata (��� �������� ������������ enum LCDCD)
void LCD_write(bool isdata, char c) {	
	//����� ������ �� ��� ���������, �.�. ����� 4-�������
	int datahigh = (c & 0xF0);
	int datalow = ((c<<4) & 0xF0); 
	
	// ��������� ��� �������, ����������� ����������� � ������� ���������� ����� RS, RW, E � ���������
	char firsthalf = 0;
	firsthalf |= (lcd_isledon<<3) | (lcd_isrwon << 1) | (isdata);
	char secondhalf = firsthalf;
	firsthalf |= datahigh;
	secondhalf |= datalow;
		
	// ��������� �������� i2c, �������� ������ � ������ ������� � ���������� �� ���� E
	i2c_start(PCF8574t);
	i2c_write(firsthalf | (1<<2));
	i2c_write(firsthalf);
	i2c_write(secondhalf | (1<<2));
	i2c_write(secondhalf);
	i2c_stop();
	// ���� ��� ������� - �������, � �� ������, ���� ���� ��� ���������� (�������� ��������� ����������� =) )
	if (!isdata) _delay_ms(2);
}

// ������� �� ������� ������ � ����� ������� �������
void LCD_clear_screen()
{
	LCD_write(LCDCOMMAND, 0x01);
}

// ������� ������������� ���
void LCD_init() {
	// �������� ����� ������ �������
	_delay_ms(1500);
	
	// ��������� 4-��� ������ (������ ������ ����� �������)
	i2c_start(PCF8574t);
	i2c_write(0x2c);
	i2c_write(0x28);
	i2c_stop();
	
	// ��������� 4-��� ������, ��� ������, ������ �������� ����������� ���������������
	LCD_write(LCDCOMMAND, 0x2a);
	
	// �������� �������, ������ ��������
	LCD_write(LCDCOMMAND, 0x0c);
	
	// ����� ������� ������, ����� ������� ��������
	LCD_write(LCDCOMMAND, 0x06);
	
	// ������� ������ � ����� ������� �������
	LCD_clear_screen();
	
	_delay_ms(100);
}


// ������� �� ����� ������� �������
void LCD_cursor_return()
{
	LCD_write(LCDCOMMAND, 0x02);
}


/*
// ����������� ������ �� ���� ������� ������
void LCD_move_cursor_right()
{
	LCD_write(LCDCOMMAND, 0x14);	
}
*/

// ����������� ������ �� x (1-20) ������� y (1-4) ������
void LCD_move_cursor(int x, int y)
{
	x--; y--;
	// 2 � 3 ������ �������� �������
	if (y==1) y=2; else if (y==2) y=1;
	int cmd = 0;
	cmd += y*20;
	cmd += x;
	if(cmd>39) {
		cmd += 24; 
	}	
	cmd |= (1<<7);
	LCD_write(LCDCOMMAND, cmd);
}

// ������ ������� ������ � ��� (���������� � �������� �����!)
void LCD_print(char* string)
{
	while(*string) LCD_write(LCDDATA, *string++);
}

// ������ ����� ������ � ��� (������� ����������� ���������)
void LCD_print_line(char* string, int line)
{
	int length = strlen(string);
	int halfmis = (20 - length)/2;
	int i =0;
	if (length <= 20)
	{
		LCD_move_cursor(1, line);
		for (int i = 20 - length - halfmis; i>0; i--) {
			LCD_print(" ");
		}
		LCD_print(string);
		for (i = 0; i<halfmis; i++) {
			LCD_print(" ");
		}
	}
	if (line == 1) LCD_move_cursor(1, 2); else if (line == 2) LCD_move_cursor(1, 3);
}


// ����� �� ����� ���������� � hex ����, ����� ��� �������
void LCD_write_hex(char data)
{
	int datahigh = (data & 0xF0);
	int datalow = ((data<<4) & 0xF0);
	char *value = 0;
	itoa(datahigh, value, 16);
	LCD_write(LCDDATA, *value);
	itoa(datalow, value, 16);
	LCD_write(LCDDATA, *value);
}


#endif /* LCD_H_ */