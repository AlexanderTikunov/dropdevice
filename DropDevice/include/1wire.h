/*
 * _1wire.h
 *
 * Created: 07.06.2017 21:22:09
 http://blablacode.ru/mikrokontrollery/442
 */ 

#ifndef ONEWIRE_H_
#define ONEWIRE_H_

#include <math.h>
#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>

/* ���������� ���� ��������� ������ */
#define THERM_PORT PORTD
#define THERM_DDR DDRD
#define THERM_PIN PIND
#define THERM_DQ PIND7
/* ������� ��� "�������� ����" � ��������� ������ �����/������ */
#define THERM_INPUT_MODE() THERM_DDR&=~(1<<THERM_DQ)
#define THERM_OUTPUT_MODE() THERM_DDR|=(1<<THERM_DQ)
#define THERM_LOW() THERM_PORT&=~(1<<THERM_DQ)
#define THERM_HIGH() THERM_PORT|=(1<<THERM_DQ)

// ����� �������
uint8_t therm_reset(){
	uint8_t i;
	// �������� ���� ���� �� 480uS
	THERM_LOW();
	THERM_OUTPUT_MODE();
	_delay_us(480);
	// �������� ����� �� 60uS
	THERM_INPUT_MODE();
	_delay_us(60);
	// �������� �������� �� ����� � ������ 480uS
	i=(THERM_PIN & (1<<THERM_DQ));
	_delay_us(420);
	// ���������� �������� (0=OK, 1=������ �� ������)
	return i;
}

// ������� �������� ����
void therm_write_bit(uint8_t bit){
	// �������� �� 1uS
	THERM_LOW();
	THERM_OUTPUT_MODE();
	_delay_us(1);
	// ���� ����� ��������� 1, ��������� ����� (���� ���, ��������� ��� ����)
	if(bit) THERM_INPUT_MODE();
	// ���� 60uS � ��������� �����
	_delay_us(60);
	THERM_INPUT_MODE();
}

// ������ ����
uint8_t therm_read_bit(void){
	uint8_t bit=0;
	// �������� �� 1uS
	THERM_LOW();
	THERM_OUTPUT_MODE();
	_delay_us(1);
	// ��������� �� 14uS
	THERM_INPUT_MODE();
	_delay_us(14);
	// ������ ���������
	if(THERM_PIN&(1<<THERM_DQ)) bit=1;
	// ���� 45 ��� � ���������� ��������
	_delay_us(45);
	return bit;
}

// ������ ����
uint8_t therm_read_byte(void){
	uint8_t i=8, n=0;
	while(i--){
		// �������� � ����� �� 1 � ��������� ��������� ��������
		n>>=1;
		n|=(therm_read_bit()<<7);
	}
	return n;
}

// ���������� ����
void therm_write_byte(uint8_t byte){
	uint8_t i=8;
	while(i--){
		// ���������� ��� � �������� ������ �� 1
		therm_write_bit(byte&1);
		byte>>=1;
	}
}

// ������� ���������� ��������
#define THERM_CMD_CONVERTTEMP 0x44
#define THERM_CMD_RSCRATCHPAD 0xbe
#define THERM_CMD_WSCRATCHPAD 0x4e
#define THERM_CMD_CPYSCRATCHPAD 0x48
#define THERM_CMD_RECEEPROM 0xb8
#define THERM_CMD_RPWRSUPPLY 0xb4
#define THERM_CMD_SEARCHROM 0xf0
#define THERM_CMD_READROM 0x33
#define THERM_CMD_MATCHROM 0x55
#define THERM_CMD_SKIPROM 0xcc
#define THERM_CMD_ALARMSEARCH 0xec

#define THERM_DECIMAL_STEPS_12BIT 625 //.0625

// ������ ����������� � �������
float therm_read_temperature(char *buffer){
	uint8_t temperature[2];
	int8_t digit;
	int8_t status;
	uint16_t decimal;
	
	status = therm_reset();
	if (status != 0) return 1000;
	therm_write_byte(THERM_CMD_SKIPROM);
	therm_write_byte(THERM_CMD_CONVERTTEMP);
	
	while(!therm_read_bit());
	
	therm_reset();
	therm_write_byte(THERM_CMD_SKIPROM);
	therm_write_byte(THERM_CMD_RSCRATCHPAD);
	
	temperature[0]=therm_read_byte();
	temperature[1]=therm_read_byte();
	therm_reset();
	
	digit=temperature[0]>>4;
	digit|=(temperature[1]&0x7)<<4;
	
	decimal=temperature[0]&0xf;
	decimal*=THERM_DECIMAL_STEPS_12BIT;
	//digit-=25.5; // ��� �������
	sprintf(buffer, "%+3d.%d�", digit, decimal/1000);
	float fvalue = digit + (decimal/1000)*0.1;
	return fvalue;
}


#endif /* ONEWIRE_H_ */