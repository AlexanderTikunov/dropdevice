/*
 * timers.h
 *
 * Created: 06.06.2017 20:24:05
 *  Author: Alexander
 */ 

#ifndef TIMERS_H_
#define TIMERS_H_

#include <avr/io.h>

// ������ ���� DRV_PUL: ����, 100��, 200��
enum DRV_PUL_States {DRV_PUL_0Gz, DRV_PUL_100Gz, DRV_PUL_200Gz};

// ������������� TIMER1 �� 500���
void TIMER1_init() {
	TCCR1A= (0<<COM1A1) | (0<<COM1A0) | (0<<COM1B1) | (1<<COM1B0) | (0<<WGM11) | (0<<WGM10);
	TCCR1B= (0<<ICNC1) | (0<<ICES1) | (0<<WGM13) | (1<<WGM12) | (0<<CS12) | (0<<CS11) | (0<<CS10);
	TIMSK1 |= (0<<ICIE1) | (0<<OCIE1B) | (0<<OCIE1A)| (0<<TOIE1);
	TCNT1=0;
	OCR1A=319;
	OCR1B=0;
}

void DRV_PUL_SetMode(int mode)
{
	switch (mode)
	{
		case DRV_PUL_200Gz:
		OCR1A=319;
		break;
		case DRV_PUL_100Gz:
		OCR1A=639;
		break;
		default:
		OCR1A=0;
		break;
	}
	if (mode != DRV_PUL_0Gz)
	TCCR1B = (0<<ICNC1) | (0<<ICES1) | (0<<WGM13) | (1<<WGM12) | (0<<CS12) | (1<<CS11) | (1<<CS10);
	else {
	TCCR1B= (0<<ICNC1) | (0<<ICES1) | (0<<WGM13) | (1<<WGM12) | (0<<CS12) | (0<<CS11) | (0<<CS10);
	PORTB &=~ (1<<PINB2);
	}
}

// ������������� TIMER0
void TIMER0_init() {
	TCCR0A= (0<<COM0A1) | (0<<COM0A0) | (0<<COM0B1) | (0<<COM0B0) | (0<<WGM01) | (0<<WGM00);
	TCCR0B= (0<<FOC0A) | (0<<FOC0B) |(0<<WGM02) |(0<<CS02) | (0<<CS01) | (1<<CS00);
	TIMSK0 = (0<<OCIE0B) | (0<<OCIE0A) | (1<<TOIE0);
	TCNT0=0;
}

#endif /* TIMERS_H_ */